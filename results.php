<?php 


$sth = $db->prepare('SELECT id
						  FROM votes');
$sth->execute(); 
$votesAll = count($sth->fetchAll());


$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('Java')); 
$votesJava = count($sth->fetchAll());

$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('C++')); 
$votesC = count($sth->fetchAll());

$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('C#')); 
$votesCS = count($sth->fetchAll());

$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('JavaScript')); 
$votesJavaScript = count($sth->fetchAll());

$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('PHP')); 
$votesPHP = count($sth->fetchAll());

$sth = $db->prepare('SELECT id
						  FROM votes
						  WHERE vote = ?');
$sth->execute(array('Other')); 
$votesOther = count($sth->fetchAll());


