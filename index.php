<?php 
include_once 'connection.php';
//include_once 'insertTestData.php';

$view_file='vote';
$view_data='';

if(isset($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
	
if(isset($_POST['Poll'])) {

$sth = $db->prepare('SELECT ip
						  FROM votes
						  WHERE ip = ?');
$sth->execute(array($_SERVER['REMOTE_ADDR']));  //$_SERVER['REMOTE_ADDR']   <-  tu jest adres hosta
$ipExists = $sth->fetchAll();

if($ipExists)
	{	
	$view_file='result';
	$view_data='juz glosowales';	
	}
else 
	{
	$sql = "INSERT INTO Votes (ip,vote) VALUES (:ip,:vote)";
	$q = $db->prepare($sql);
	$q->execute(array(':ip'=>$ip,
	                  ':vote'=>strtolower($_POST['Poll'])));	
	$view_file='result';
	$view_data='Dziekuje za oddanie glosu';

	}
}


include_once 'view/' . $view_file .'.php';
