<?php

$votes = array(
                  array('ip' => '192.168.200.201',
                        'vote' => 'Java'),
                  array('ip' => '192.168.162.201',
                        'vote' => 'C#'),
				  array('ip' => '192.168.200.206',
                        'vote' => 'JavaScript'),
                  array('ip' => '192.168.162.211',
                        'vote' => 'PHP'),
				  array('ip' => '192.168.55.201',
                        'vote' => 'C#'),
                  array('ip' => '192.168.44.201',
                        'vote' => 'PHP'),
				  array('ip' => '192.168.30.201',
                        'vote' => 'Java'),
                  array('ip' => '192.168.22.201',
                        'vote' => 'Java'),
				  array('ip' => '192.168.11.201',
                        'vote' => 'C++'),
                  array('ip' => '192.168.67.201',
                        'vote' => 'PHP'),
				  array('ip' => '192.168.54.201',
                        'vote' => 'Other'),
                  array('ip' => '192.168.32.201',
                        'vote' => 'PHP'),
                  array('ip' => '192.168.57.201',
                        'vote' => 'JavaScript' ));
						
	$insert = "INSERT INTO votes (ip, vote) 
                VALUES (:ip, :vote)";
    $stmt = $db->prepare($insert);
 
    // Bind parameters to statement variables
    $stmt->bindParam(':ip', $ip);
    $stmt->bindParam(':vote', $vote);
 
    // Loop thru all votes and execute prepared insert statement
    foreach ($votes as $m) {
      // Set values to bound variables
      $ip = $m['ip'];
      $vote = $m['vote'];
 
      // Execute statement
      $stmt->execute();
    }
 
 